from scoretracker.models import Player, Game, GameData, SimplePlayer
from django.contrib import admin


class GameDataInline(admin.TabularInline):
    model = GameData
    extra = 3

class GameAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,      {'fields': ['date']}),
    ]
    inlines = [GameDataInline]

class PlayerAdmin(admin.ModelAdmin):
    list_display = ('name', 'Goals', 'Games', 'Ratio')
    

admin.site.register(Game, GameAdmin)

admin.site.register(Player, PlayerAdmin)

admin.site.register(SimplePlayer)

admin.site.register(GameData)
