from django.db import models

# Create your models here.
class SimplePlayer(models.Model):
	name = models.CharField(max_length=50, unique=True)
	goals = models.IntegerField()
	assists = models.IntegerField()
	games = models.IntegerField()

	
	@property
	def points(self):
		return self.goals + self.assists
	

	@property
	def ratio(self):
		if self.points() == 0 or self.games == 0:
			return 0.0
			
		return (self.Points/self.Games)
		
	def __unicode__(self):
		return '%s' %(self.name)
	

class Player(models.Model):
	name = models.CharField(max_length=30)
	games = models.ManyToManyField('Game', through='GameData', null=True, blank=True)
	
	
	@property
	def gameslist(self):
		gameIds = []
		for gd in self.gamedata_set.all():
			gameIds.append(gd.game_id)
	
		return gameIds
	
	@property
	def Goals(self):
		goals = 0
		for game in self.gamedata_set.all():
			goals = goals + game.goals

		return goals

	@property
	def Assists(self):
		assists = 0
		for game in self.gamedata_set.all():
			assists = assists + game.assists

		return assists

	@property
	def Games(self):
		return self.gamedata_set.count()
	#getGames = property(getGames, doc='Games')
	
	@property
	def Points(self):
		return self.Assists + self.Goals
	
	@property
	def Ratio(self):
		if self.Points == 0 or self.Games == 0:
			return 0
			
		return (self.Points/self.Games)

	def __unicode__(self):
		return self.name



class GameData(models.Model):
	player = models.ForeignKey('Player')
	game = models.ForeignKey('Game')

	goals = models.IntegerField()
	assists = models.IntegerField()

	def __unicode__(self):
		return '%r %r' %(self.player.name, self.game.formatDate())

class Game(models.Model):
	date = models.DateTimeField()
	#players = models.ManyToManyField('Player', through='GameData')
	
	@property
	def getPlayers(self):
		players = []
		for gd in player_set.all:
			players.append(gd.player.name)

		return players

	def formatDate(self):
		return self.date.strftime('%d %b %Y %H:%M')
	
	def  __unicode__(self):
		return '%d, %r' %(self.id, self.formatDate())
