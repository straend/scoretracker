from scoretracker.models import SimplePlayer,Player, Game, GameData
from djangorestframework.resources import ModelResource
from djangorestframework.reverse import reverse

from djangorestframework.views import ListOrCreateModelView, InstanceModelView

from django.conf.urls.defaults import patterns, url

from djangorestframework.permissions import IsAuthenticated



class PlayerResource(ModelResource):
    """
    Exposes all players for the world in a RESTfull interface
    """
    permissions = (IsAuthenticated, )
    model = SimplePlayer
    fields = [
        'id', 'name','goals', 'assists','games', 'ratio', 
    ]
