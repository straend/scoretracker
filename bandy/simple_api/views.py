from djangorestframework.resources import FormResource
from djangorestframework.response import Response
from djangorestframework.renderers import BaseRenderer
from djangorestframework.reverse import reverse
from djangorestframework.views import View
from djangorestframework.views import ListOrCreateModelView, InstanceModelView

from djangorestframework import status
from djangorestframework.permissions import IsAuthenticated

from scoretracker.models import SimplePlayer
from simple_api.resources import *


class PlayersRoot(View):
    """
    Returns a list of players
    Allows one to add Player/update players info
    """

    """
    allowed_methods = ["GET", "POST", "PUT",]
    permissions = (IsAuthenticated, )

    resource = PlayerResource
    """
    permissions = (IsAuthenticated, )
    def get(self, request, player_id=None):
        if player_id == None:
            # Creates a list of urls for players
            #ids = [ p.id for p in SimplePlayer.objects.all() ]
            #return [reverse('player-info', request=request, args=[player_id]) for player_id in ids]
            return SimplePlayer.objects.all()
        else:
            #if SimplePlayer.
            return SimplePlayer.objects.get(pk=player_id)

    def put(self, request, player_id=None):
        if player_id == None:
            """
            Update many players
            """
            updatedPlayers = 0
            createdPlayers = 0
            requested = len(self.CONTENT)
            #return self.CONTENT

            for player in self.CONTENT:
                dbPlayer = SimplePlayer.objects.filter(name=player["name"])
                if len(dbPlayer) > 0:
                    # Update existing player
                    dbPlayer = dbPlayer[0]
                    dbPlayer.goals += player.get("goals",0)
                    dbPlayer.assists += player.get("assists",0)
                    dbPlayer.games += 1
                    updatedPlayers +=1

                else:
                    # Create Player
                    dbPlayer = SimplePlayer(
                        name=player["name"], 
                        goals=player.get("goals",0), 
                        assists=player.get("assists",0), 
                        games=1
                    )
                    createdPlayers += 1
                
                dbPlayer.save()
                

            return {'updated': updatedPlayers, 'created': createdPlayers, 'request': requested }
        else:
            player = SimplePlayer.objects.get(pk=player_id)

            return "PUT request to Player %s: %s, with content: %s" % (player_id, player.name, self.CONTENT)


