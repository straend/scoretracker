from django.conf.urls.defaults import patterns, url, include
from djangorestframework.views import ListOrCreateModelView, InstanceModelView

from api.resources import *

from simple_api.views import *


urlpatterns = patterns('',
    url(r'^auth/', include('djangorestframework.urls', namespace='djangorestframework')),
	
    url(r'^players/$', PlayersRoot.as_view(), name='players-list'),
    url(r'^players/(?P<player_id>[0-9]+)$', PlayersRoot.as_view(), name='player-info'),
   
 )