from scoretracker.models import Player, Game, GameData
from djangorestframework.resources import ModelResource
from djangorestframework.reverse import reverse

from djangorestframework.views import ListOrCreateModelView, InstanceModelView

from django.conf.urls.defaults import patterns, url

from djangorestframework.permissions import IsAuthenticated



class PlayerResource(ModelResource):
    """
    Exposes all players for the world in a RESTfull interface
    """
    permissions = (IsAuthenticated, )
    model = Player
    fields = [
     #   'url',
        'id', 'name','Goals', 'Assists','Games', 'Ratio', 
        'gameslist',
        #('games', ('game', ('id'))),        
    ]

    #def url(self, instance):
    #    return reverse('model-resource-instance',
    #                   kwargs={'id': instance.id},
    #                   request=self.request)
    

class GameResource(ModelResource):
    model = Game
    fields = ['id', 'date', 'getPlayers']
    #fields = [
    #   'name','Goals', 'Assists','Games', 
    #   ('played_games', ('games', 'game__id'))
    #]

class GameDataResource(ModelResource):
    model = GameData
    #exclude = ('player')
    fields =( 
        ('game', ('game','date')), 
        ('player', ('player', 'name')),
         'goals', 'assists'
    )


