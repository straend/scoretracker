from django.conf.urls.defaults import patterns, url, include
from djangorestframework.views import ListOrCreateModelView, InstanceModelView
from api.resources import *

from api.views import *


#my_model_list = ListOrCreateModelView.as_view(resource=MyResource)
#my_model_instance = InstanceModelView.as_view(resource=MyResource)

#game_model_list = ListOrCreateModelView.as_view(resource=GameR)
#game_model_instance = InstanceModelView.as_view(resource=GameR)

#data_model_list = ListOrCreateModelView.as_view(resource=GameDataR)
#data_model_instance = InstanceModelView.as_view(resource=GameDataR)

## Djangorestframwork
urlpatterns = patterns('',
    url(r'^auth/', include('djangorestframework.urls', namespace='djangorestframework')),
	
    #url(r'^p/$', my_model_list, name='model-resource-root'),
    #url(r'^p/(?P<id>[0-9]+)/$', my_model_instance, name='model-resource-instance'),
    
    #url(r'^g/$', game_model_list, name='game-resource-root'),
    #url(r'^g/(?P<id>[0-9]+)/$', game_model_instance, name='game-resource-instance'),

    #url(r'^gd/$', data_model_list, name='gamedata-resource-root'),
    #url(r'^gd/(?P<id>[0-9]+)/$', data_model_instance, name='gamedata-resource-instance'),

    url(r'^players$', PlayersRoot.as_view(), name='players-root'),
    url(r'^games$', GamesRoot.as_view(), name='games-root'),
    
    url(r'^players/(?P<id>[0-9]+)$', PlayersRoot.as_view(), name='players-root'),
    url(r'^games/(?P<id>[0-9]+)$', GamesRoot.as_view(), name='games-root'),
    

    url(r'^gamedata$', GameDataRoot.as_view(), name='gamedata-root'),
    url(r'^gamedata/game/(?P<game_id>[0-9]+)$', GameDataRoot.as_view(), name='gamedata-root'),
    url(r'^gamedata/player/(?P<player_id>[0-9]+)$', GameDataRoot.as_view(), name='gamedata-root'),
    
    url(r'^pplayers$', PlayersRootPub.as_view(), name='players-root'),
 )