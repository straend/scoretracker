from djangorestframework.resources import FormResource
from djangorestframework.response import Response
from djangorestframework.renderers import BaseRenderer
from djangorestframework.reverse import reverse
from djangorestframework.views import View
from djangorestframework.views import ListOrCreateModelView, InstanceModelView

from djangorestframework import status
from djangorestframework.permissions import IsAuthenticated

#from scoretracker.models import *
from api.resources import *


class PlayersRoot(ListOrCreateModelView):
    """
    Returns a list of players
    """
    allowed_methods = ["GET", "POST"]
    permissions = (IsAuthenticated, )

    resource = PlayerResource

class PlayersRootPub(ListOrCreateModelView):
    """
    Returns a list of players
    """
    allowed_methods = ["GET"]
    
    resource = PlayerResource

class GamesRoot(ListOrCreateModelView):
    """
    Returns a list of Games
    """
    allowed_methods = ["GET", "POST"]
    permissions = (IsAuthenticated, )

    resource = GameResource

class GameDataRoot(ListOrCreateModelView):
    """
    Returns info for all games
    """
    allowed_methods = ["GET", "POST"]
    permissions = (IsAuthenticated, )

    resource = GameDataResource

